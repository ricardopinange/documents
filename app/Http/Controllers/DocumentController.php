<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Repositories\DocumentRepository;
use App\Http\Requests\DocumentRequest;
use PDF;

class DocumentController extends Controller
{
    private $documentRepository;

    public function __construct(DocumentRepository $documentRepository)
    {
        $this->documentRepository = $documentRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $data = $this->documentRepository->all();
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(DocumentRequest $request)
    {
        try {
            $data = null;
            DB::transaction(function () use ($request, &$data) {
                $new = $this->documentRepository->store($request->validated());
                $data = $this->saveFile($request, $new);
            });
            return $this->responseSuccess($data, 'Document successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        try {
            $data = $this->documentRepository->find($id);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(DocumentRequest $request, string $id)
    {
        try {
            $data = $this->documentRepository->update($request->validated(), $id);
            return $this->responseSuccess($data, 'Document successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $this->documentRepository->destroy($id);
            return $this->responseSuccess([], 'Document successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Download the specified resource from storage.
     */
    public function download(string $id)
    {
        try {
            $data = $this->documentRepository->find($id);

            if (!isset($data)) {
                $message = "Register not found";
                throw new \Exception($message, Response::HTTP_NOT_FOUND);
            }

            $is_pdf = $this->convertToPDF($data);

            if ($is_pdf) {
                $file_extension = 'pdf';
            } else {
                $file_extension = $data->extension;
            }

            $file = Storage::disk('public')->get("{$data->path}/{$data->id}.{$file_extension}");

            $headers = [
                "Content-Type" => "application/{$file_extension}",
                "Content-Disposition" => "attachment; filename={$data->name}.{$file_extension};"
            ];
            return response()->stream(function () use ($file) {
                echo $file;
            }, 200, $headers);

        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    /**
     * Save file
     *
     * @param array $data
     * @return array
     */
    private function saveFile($request, $document)
    {
        if ($request->hasFile('file')) {
			$path = "/document";
            $file = $request->file('file');
            $file_extension = $file->extension();

            $data['name'] = $document->name;
            $data['extension'] = $file_extension;
            $data['type'] = $file->getClientMimeType();
            $data['size'] = $file->getSize();
			$data['path'] = $path;

            $document = $this->documentRepository->update($data, $document->id);
            $file_name = "{$path}/{$document->id}.{$file_extension}";

            if (!Storage::disk('public')->put($file_name, file_get_contents($file))) {
                throw new \Exception('Could not save file', Response::HTTP_INTERNAL_SERVER_ERROR);
			}

			return $document;
        } else {
            throw new \Exception('File not found', Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Convert to PDF
     *
     * @param array $data
     * @return boolean
     */
    private function convertToPDF($document)
    {
        $is_pdf = false;
        $input_file = Storage::path("/public/{$document->path}/{$document->id}.{$document->extension}");
        $output_file = Storage::path("/public/{$document->path}/{$document->id}.pdf");

        switch ($document->extension) {
            case 'pdf':
                $is_pdf = true;
                break;
            case 'doc':
            case 'docx':
                $is_pdf = $this->convertWordToPDF($input_file, $output_file);
                break;
            case 'html':
            case 'txt':
                $is_pdf = $this->convertHtmlToPDF($input_file, $output_file);
                break;
            case 'bmp':
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
            case 'svg':
            case 'webp':
                $is_pdf = $this->convertImageToPDF($input_file, $output_file);
                break;
        }

        return $is_pdf;
    }

    /**
     * Convert Word to PDF
     *
     * @param string $input_file, $output_file
     * @return boolean
     */
    public function convertWordToPDF($input_file, $output_file)
    {
        $domPdfPath = base_path('vendor/dompdf/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
        $Content = \PhpOffice\PhpWord\IOFactory::load($input_file);
        $PDFWriter = \PhpOffice\PhpWord\IOFactory::createWriter($Content,'PDF');
        $PDFWriter->save($output_file);

        return true;
    }

    /**
     * Convert Html to PDF
     *
     * @param string $input_file, $output_file
     * @return boolean
     */
    public function convertHtmlToPDF($input_file, $output_file)
    {
        $html = file_get_contents($input_file);
        return PDF::loadHTML($html)->setWarnings(false)->save($output_file);
    }

    /**
     * Convert Image to PDF
     *
     * @param string $input_file, $output_file
     * @return boolean
     */
    public function convertImageToPDF($input_file, $output_file)
    {
        $html = "<img src=\"{$input_file}\" style=\"max-width: 100%;\" />";
        return PDF::loadHTML($html)->setWarnings(false)->save($output_file);
    }
    
}
