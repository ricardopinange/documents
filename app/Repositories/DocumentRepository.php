<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class DocumentRepository implements RepositoryInterface
{
    public $request;

    public function __construct()
    {
        $this->request = app(Request::class);
    }

    public function all()
    {
        $query = Document::select();

        if (isset($this->request->search)) {
            $query->where('name', 'LIKE', '%' . $this->request->search . '%');
        }

        if (isset($this->request->id)) {
            $query->where('id', $this->request->id);
        }

        if (isset($this->request->start_created_date)) {
            $query->where('created_at', '>=', $this->request->start_created_date);
        }

        if (isset($this->request->end_created_date)) {
            $query->where('created_at', '<=', $this->request->end_created_date);
        }

        if (isset($this->request->per_page)) {
            $per_page = in_array($this->request->per_page, [12,24,36])
                ? $this->request->per_page
                : 12;
        }

        $data = $query->orderBy('document.created_at', 'DESC')
            ->paginate($per_page ?? 12);

        return $data;
    }

    public function store($data)
    {
        if ($this->validate($data['name'])) {
            $message = "Document already registered";
            throw new \Exception($message, Response::HTTP_BAD_REQUEST);
        }

        $id = Document::create($data)->id;
        return Document::find($id);
    }

    public function find($id)
    {
        return Document::find($id);
    }

    public function findLastOneByFilter(array $filter)
    {
        return Document::where($filter)->latest()->first();
    }

    public function update($data, $id)
    {
        $document = Document::find($id);

        if (!isset($document)) {
            $message = "Register not found";
            throw new \Exception($message, Response::HTTP_NOT_FOUND);
        }

        if ($this->validate($data['name'], $id)) {
            $message = "Document already registered";
            throw new \Exception($message, Response::HTTP_BAD_REQUEST);
        }

        $document->update($data);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = Document::find($id);

        if (!isset($data)) {
            $message = "Register not found";
            throw new \Exception($message, Response::HTTP_NOT_FOUND);
        }

        $data->delete();
    }

    public function validate(string $name, string $id = NULL)
    {
        $query = Document::where('name', $name);

        if (isset($id)) {
            $query->where('id', '!=', $id);
        }

        return $query->first();
    }
}
